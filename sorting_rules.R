sorting_rules <- function(sorting_rule){
  ss <- strsplit(sorting_rule, "\\|")
  ss <- lapply(ss, function(x) strsplit(x, "\\["))
  
  col_names <- sapply(ss[[1]], function(x) x[1])
  
  values <- sapply(ss[[1]], function(x) x[2])
  values <- sapply(values, function(x) if(!is.na(x)) {gsub("\\]", "", x)} else {"empty"})
  
  df_rt <- data.frame(fields = col_names, values = values, stringsAsFactors = F)
  df_rt[df_rt$fields == "VOZRAST", "values"] <- "Взрослые"
  df_rt[df_rt$fields == "POL", "values"] <- "Мужской,Женский,Унисекс"
  
  library(tidyr)
  max_fields <- max(lengths(regmatches(df_rt$values, gregexpr("\\,", df_rt$values)))) + 1
  df_rt <- separate(df_rt, values, paste("col", 1:max_fields, sep = ""), sep = "\\,", fill = "right")
  tdf <- t(df_rt)
  x <- tdf[-1, ]
  x <- rbind(x, rep("other", ncol(x)))
  x <- as.data.frame(x)
  x <- x[, ncol(x):1]
  x <- lapply(x, function(x)  x[!is.na(x)])
  x <- expand.grid(as.list(x))
  x <- x[complete.cases(x), ]
  x <- x[, ncol(x):1]
  names(x) <- tdf[1, ]
  x$sort <- nrow(x):1
  x
}
