describe_branch <- function(csv_path, mdb_connection){
  test <- read.csv(csv_path, header=FALSE, sep=";", stringsAsFactors = F)
  table_header <- test[1,]
  df <- test[-1, ]
  
  source("https://bitbucket.org/klovshot/branch_sorting/raw/master/get_rt.R", encoding = "UTF-8")
  rt <- get_rt(mdb_connection)
  
  df <- merge(df, rt, by.x = "V1", by.y = "id", all.x = T)
  
  df <- aggregate(V1 ~ KOLLEKTSIYA_KIS + VOZRAST + POL + GRUPPA_TOVAROV + TOVARNAYA_GRUPPA, df, length)
  names(df)[names(df) == "V1"] <- "number_of_products"
  df[rev(order(df$number_of_products)), ]
}