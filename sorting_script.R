sorting_script <- function(csv_path, sorting_rule, mdb_connection) {
  test <- read.csv(csv_path, header=FALSE, sep=";", stringsAsFactors = F)
  table_header <- test[1,]
  df <- test[-1, ]

  source("https://bitbucket.org/klovshot/branch_sorting/raw/master/get_rt.R", encoding = "UTF-8")
  rt <- get_rt(mdb_connection)


  df <- merge(df, rt, by.x = "V1", by.y = "id", all.x = T)

  source("https://bitbucket.org/klovshot/branch_sorting/raw/master/sorting_rules.R", encoding = "UTF-8") 
  sr <- sorting_rules(sorting_rule)

  d <- setdiff(df$TOVARNAYA_GRUPPA, sr$TOVARNAYA_GRUPPA)
  if(length(d) > 0) message(paste("Товарные группы, которые не использованы в правилах, —", paste(d, collapse = ", ")))
  d2 <- setdiff(df$TOVARNAYA_PODGRUPPA, sr$TOVARNAYA_PODGRUPPA)
  if(length(d2) > 0) message(paste("Товарные подгруппы, которые не использованы в правилах, —", paste(d2, collapse = ", ")))
  
  
  
  for (i in names(sr[-length(sr)])){
    df[, i][!(df[, i] %in% sr[, i])] <- "other"
  }
  
  pre_res <- merge(df, sr)
  pre_res$V3 <- pre_res$sort
  res <- pre_res[, c("V1", "V2", "V3", "V4")]
  names(res) <- paste(table_header[1, ])
  write.csv(res, paste(strsplit(csv_path, "\\.")[[1]][1], "_sorted.csv", sep = ""), row.names = F)
}